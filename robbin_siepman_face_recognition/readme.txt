To run: open terminal in this folder.
Provide your training set in the resources/raw folder. 
For more information see resources/folders_explained.txt

run cmake to generate makefile:
cmake .

make the executable:
make siepman_face_recognition

run the executable
./siepman_face_recognition

OR just import this project in Clion and run it from there.

===Troubleshooting===
Requires opencv 3.1.0 with contrib (my version, but opencv 3.0+ should be fine)
check version with: pkg-config --modversion opencv

Also requires an opencv install with additional libraries, like so:
https://gist.github.com/SSARCandy/fc960d8905330ac695e71e3f3807ce3d


