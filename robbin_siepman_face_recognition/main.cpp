#include "vision/recognition/EigenFaces.h"

int main() {
    EigenFaces eigenFaces;
    eigenFaces.init();

    eigenFaces.checkFilesWithUi();

    return 0;
}