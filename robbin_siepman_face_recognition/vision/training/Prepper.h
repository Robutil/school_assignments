#ifndef VISION_ASSIGNMENTS_PREPPER_H
#define VISION_ASSIGNMENTS_PREPPER_H


#include "../utilities/Robutil.h"
#include "../detection/FaceDetector.h"
#include "../utilities/CheckImage.h"

#define THRESHOLD_SIZE 1 //Must be > 0

class Prepper {
private:
    std::string path = "resources/";
    std::string raw_path = path + "raw/";
    std::string formatted_path = path + "formatted/";

    Robutil robutil;
    FaceDetector faceDetector;
    CheckImage checkImage;

    cv::Rect getLargestFace(std::vector<cv::Rect> &faces);

    void storeLargestDetectedFace(const std::string &filename, const std::string &folder_name);

    std::vector<std::string> getCleanFolderNames(std::vector<std::string> &folders);

public:
    std::vector<std::string> createFormattedImages();
};


#endif //VISION_ASSIGNMENTS_PREPPER_H
