#include <opencv2/opencv.hpp>

#include "Prepper.h"

cv::Rect Prepper::getLargestFace(std::vector<cv::Rect> &faces) {
    cv::Rect largestDetectedFace;
    for (const cv::Rect &face: faces) {
        if (face.area() > largestDetectedFace.area()) {
            largestDetectedFace = face;
        }
    }
    return largestDetectedFace;
}

std::vector<std::string> Prepper::getCleanFolderNames(std::vector<std::string> &folders) {
    std::vector<std::string> clean_names;
    for (const std::string &name: folders) {
        clean_names.push_back(robutil.getCleanName(name));
    }
    return std::move(clean_names);
}

void Prepper::storeLargestDetectedFace(const std::string &filename, const std::string &folder_name) {
    cv::Mat img = cv::imread(filename);

    std::vector<cv::Rect> detectedFaces = faceDetector.checkForFaces(img);
    std::cout << filename << ", detected " << detectedFaces.size() << " face(s)\n";

    //Since this is the training set, we only expect one face
    cv::Rect largestDetectedFace = getLargestFace(detectedFaces);

    //Cut out face and format it (no detected face will be width=0, height=0)
    if (largestDetectedFace.width > THRESHOLD_SIZE && largestDetectedFace.height > THRESHOLD_SIZE) {

        //Cut image
        cv::Mat cropped_image = checkImage.cropImage(img, largestDetectedFace);

        //Store image
        std::string clean_image_name = robutil.getCleanName(filename);
        cv::imwrite(formatted_path + folder_name + "/" + clean_image_name, cropped_image);
    }
}

/**
 * Returns list of of folder names that were formatted
 * */
std::vector<std::string> Prepper::createFormattedImages() {
    std::vector<std::string> raw_folders = robutil.listdir(raw_path); //Contains unedited images
    std::vector<std::string> formatted_folders = robutil.listdir(formatted_path); //Contains properly formatted images
    std::vector<std::string> formatted_folders_clean = getCleanFolderNames(formatted_folders); //No paths

    std::vector<std::string> newly_formatted_folders;
    for (const std::string &raw_folder: raw_folders) {
        std::string folder_name = robutil.getCleanName(raw_folder); //No path

        //If the folder was not already formatted
        if (!robutil.has(formatted_folders_clean, folder_name)) {
            newly_formatted_folders.push_back(folder_name);

            std::string command = "mkdir " + formatted_path + folder_name;
            system(command.c_str()); //TODO: make this neat

            //Create formatted image from every raw image
            std::vector<std::string> images = robutil.listdir(raw_folder);
            for (const std::string &image_name: images) {
                storeLargestDetectedFace(image_name, folder_name);
            }
        }
    }

    return newly_formatted_folders;
}
