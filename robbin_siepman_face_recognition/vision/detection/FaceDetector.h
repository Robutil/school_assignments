#ifndef VISION_ASSIGNMENTS_FACEDETECTOR_H
#define VISION_ASSIGNMENTS_FACEDETECTOR_H

#define THRESHOLD_RATIO 80.0

#include <vector>
#include <opencv2/opencv.hpp>

struct FaceDetectorRect {
    int x1;
    int y1;
    int x2;
    int y2;
};

class FaceDetector {
private:
    std::string default_cascade_file = "resources/other/haarcascade_frontalface_alt.xml";
    cv::CascadeClassifier cascade_classifier;

    void init();

    FaceDetectorRect toUsableFormat(const cv::Rect &a);

public:
    FaceDetector();

    explicit FaceDetector(const std::string &default_cascade_file);

    std::vector<cv::Rect> checkForFaces(cv::Mat &img);
};


#endif //VISION_ASSIGNMENTS_FACEDETECTOR_H
