#include <cmath>

#include "FaceDetector.h"

FaceDetector::FaceDetector(const std::string &default_cascade_file) : default_cascade_file(default_cascade_file) {
    init();
}

FaceDetector::FaceDetector() {
    init();
}

/**
 * Cascade classifier requires init file
 * */
void FaceDetector::init() {
    cascade_classifier.load(default_cascade_file);
}

/**
 * Transforms a cv::Rect to a more friendly struct with two x,y positions
 * */
FaceDetectorRect FaceDetector::toUsableFormat(const cv::Rect &a) {
    return {
            a.x, a.y, a.x + a.width, a.y + a.height
    };
}

/**
 * Gives a percentage ranging from 0-100 on how much two cv::Rect's overlap
 * */
double calculateRectOverlapRatio(const cv::Rect &a, const cv::Rect &b) {
    FaceDetectorRect rect_a = {
            a.x, a.y, a.x + a.width, a.y + a.height
    };
    FaceDetectorRect rect_b = {
            b.x, b.y, b.x + b.width, b.y + b.height
    };

    int x_overlap = MAX(0, MIN(rect_a.x2, rect_b.x2) - MAX(rect_a.x1, rect_b.x1));
    int y_overlap = MAX(0, MIN(rect_a.y2, rect_b.y2) - MAX(rect_a.y1, rect_b.y1));

    double overlap_area = x_overlap * y_overlap;
    double rect_union = a.area() + b.area() - overlap_area;

    return overlap_area / rect_union;
}

/**
 * Checks a provided image for faces. Image must be with full RGB colors, do NOT do grey
 * conversion, that is done in this function.
 * */
std::vector<cv::Rect> FaceDetector::checkForFaces(cv::Mat &img) {
    //Convert image for processing
    cv::Mat grey_image;
    cv::cvtColor(img, grey_image, cv::COLOR_BGR2GRAY);
    cv::equalizeHist(grey_image, grey_image); //TODO: check how much this impacts performance (success rate)

    //Detect faces according to xml file from init
    std::vector<cv::Rect> faces;
    cascade_classifier.detectMultiScale(grey_image, faces, 1.1, 2, 0 | cv::CASCADE_SCALE_IMAGE, cv::Size(30, 30));

    //If multiple faces are detected around thesame area, use the biggest detected face
    std::vector<cv::Rect> detectedFaces;
    for (int i = 0; i < faces.size(); ++i) {
        //Get a detected face
        const cv::Rect &candidate_face = faces.at(static_cast<unsigned long>(i));
        const struct FaceDetectorRect candidate_face_neat = toUsableFormat(candidate_face);
        bool detected_better_candidate = false;

        //Check it against the other faces
        for (int j = 0; j < faces.size(); ++j) {
            const cv::Rect &stored_face = faces.at(static_cast<unsigned long>(i));
            const struct FaceDetectorRect stored_face_neat = toUsableFormat(stored_face);

            //Check if candidate is inside a stored face
            if (candidate_face_neat.x1 > stored_face_neat.x1 && candidate_face_neat.x2 < stored_face_neat.x2 &&
                candidate_face_neat.y1 < stored_face_neat.y1 && candidate_face_neat.y2 < stored_face_neat.y2) {
                detected_better_candidate = true; //Current candidate is smaller and inside another face
                break;
            }

            //Check if stored is inside candidate face
            if (stored_face_neat.x1 > candidate_face_neat.x1 && stored_face_neat.x2 < candidate_face_neat.x2 &&
                stored_face_neat.y1 < candidate_face_neat.y1 && stored_face_neat.y2 < candidate_face_neat.y2) {
                //We are the better candidate, the stored face is inside our face

            } else {
                //Check how much the candidate and stored face overlap
                double overlap_ratio = calculateRectOverlapRatio(candidate_face, stored_face);
                if (overlap_ratio > THRESHOLD_RATIO) {
                    if (stored_face.area() > candidate_face.area()) {
                        detected_better_candidate = true; //Bigger face relatively close by, that's the better candidate
                        break;
                    }
                } else {
                    //We are still the better candidate
                }
            }
        }

        if (!detected_better_candidate) {
            detectedFaces.push_back(candidate_face);
        }
    }

    return std::move(detectedFaces);
}
