#ifndef VISION_ASSIGNMENTS_EIGENFACES_H
#define VISION_ASSIGNMENTS_EIGENFACES_H

#include <opencv2/opencv.hpp>
#include <opencv2/face.hpp>

#include "../../main.h"
#include "../utilities/Robutil.h"

#define NUMBER_OF_PRINCIPAL_COMPONENTS 20

class EigenFaces {
private:
    cv::Ptr<cv::face::FaceRecognizer> model = cv::face::EigenFaceRecognizer::create(NUMBER_OF_PRINCIPAL_COMPONENTS);
    std::map<int, std::string> label_map;

    std::string path = "resources/";
    std::string path_formatted = path + "formatted/";
    std::string path_check = path + "checked/";
    std::string path_train_brain = path + "other/training_brain.dat";

    Robutil robutil;

public:
    void init();

    void loadTrainingBrainFile();

    void createNewTrainingBrain();

    void dumpTrainingBrain();

    void checkFiles();

    void checkFilesWithUi();
};


#endif //VISION_ASSIGNMENTS_EIGENFACES_H
