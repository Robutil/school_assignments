#include "EigenFaces.h"
#include "../utilities/CheckImage.h"
#include "../training/Prepper.h"

#include "opencv2/core.hpp"
#include "opencv2/face.hpp"
#include "opencv2/highgui.hpp"

void EigenFaces::init() {
    std::cout << "[EigenFaces](init): Checking for new files." << std::endl;
    Prepper prepper;
    std::vector<std::string> new_folders = prepper.createFormattedImages();
    bool trainingDataSetExists = robutil.checkIfFileExists(path_train_brain);

    if (!new_folders.empty()) {
        std::cout << "[EigenFaces](init): Found new training files, retraining!" << std::endl;
        createNewTrainingBrain();

    } else if (!trainingDataSetExists) {
        std::cout << "[EigenFaces](init): No training file found, retraining." << std::endl;
        createNewTrainingBrain();

    } else {
        std::cout << "[EigenFaces](init): Training file found, skipping training." << std::endl;
        loadTrainingBrainFile();
    }
}

void EigenFaces::loadTrainingBrainFile() {
    //Load brain
    model->read(path_train_brain);

    //Load labels
    std::vector<std::string> folders = robutil.listdir(path_formatted);
    int label_counter = 0;
    for (const std::string &folder: folders) {
        label_counter++;
        label_map[label_counter] = robutil.getCleanName(folder);
    }
}

void EigenFaces::createNewTrainingBrain() {
    std::vector<std::string> folders = robutil.listdir(path_formatted);

    std::vector<cv::Mat> images;
    std::vector<int> labels;
    int label_counter = 0;

    for (const std::string &folder: folders) {
        label_counter++;
        label_map[label_counter] = robutil.getCleanName(folder);

        std::cout << "Training from: " << folder << std::endl;

        std::vector<std::string> image_names = robutil.listdir(folder + "/");
        for (const std::string &img_name: image_names) {
            cv::Mat tmp = cv::imread(img_name, cv::IMREAD_GRAYSCALE);

            //We need to make sure each image has the same dimensions, otherwise the model crashes
            if (tmp.rows == FORMATTED_IMG_HEIGHT && tmp.cols == FORMATTED_IMG_WIDTH) {
                images.push_back(tmp);
                labels.push_back(label_counter);
            }
        }
    }


    //This can take quite a while
    model->train(images, labels);
    dumpTrainingBrain();
}

void EigenFaces::dumpTrainingBrain() {
    model->write(path_train_brain);
}

/**
 * Checks image files in check folder, these files must be cropped faces
 * */
void EigenFaces::checkFiles() {
    std::vector<std::string> files = robutil.listdir(path_check);

    for (const std::string &file_name: files) {
        std::cout << "Predicting: " << file_name << "\n";
        cv::Mat img = cv::imread(file_name, cv::IMREAD_GRAYSCALE);

        int prediction = -1;
        double confidence = 0.0;
        model->predict(img, prediction, confidence);

        if (prediction == -1) {
            std::cout << "Did not find a matching face" << std::endl;
        } else {
            std::cout << "Prediction: " << label_map[prediction] << std::endl;
            std::cout << "Confidence: " << confidence << std::endl;
        }
    }
}

/**
 * Checks image file by cropping possible faces and do a recognize attempt
 * */
void EigenFaces::checkFilesWithUi() {
    std::vector<std::string> files = robutil.listdir(path_check);

    CheckImage checkImage;
    for (const std::string &file_name: files) {
        std::cout << "[EigenFaces](checkFilesWithUi): Predicting: " << file_name << "\n";

        std::vector<cv::Mat> possibleFaces = checkImage.checkForFaces(file_name);
        for (const cv::Mat &face: possibleFaces) {
            int prediction = -1;
            double confidence = 0.0;

            model->predict(face, prediction, confidence);

            if (prediction != -1) {
                std::cout << "[EigenFaces](checkFilesWithUi): Prediction: " << label_map[prediction] << std::endl;
                std::cout << "[EigenFaces](checkFilesWithUi): Confidence: " << confidence << std::endl;

                std::string window_name = label_map[prediction] + ", acc: " + std::to_string(confidence);
                cv::namedWindow(window_name, cv::WINDOW_AUTOSIZE);// Create a window for display.
                cv::imshow(window_name, face);                   // Show our image inside it.

                cv::waitKey(0);
            }
        }
    }
}


