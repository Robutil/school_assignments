#ifndef INVERSE_KINEMATICS_IMAGELOADER_H
#define INVERSE_KINEMATICS_IMAGELOADER_H

#include <iostream>
#include <vector>
#include <opencv2/opencv.hpp>

using namespace cv;

class ImageLoader {
private:
    int counter = 0;
    std::string path;
    std::string project_location;
public:
    std::vector<Mat> images;
    std::vector<std::string> image_names;

    explicit ImageLoader(const std::string &project_location);
    bool load(std::string &folder);
};


#endif //INVERSE_KINEMATICS_IMAGELOADER_H
