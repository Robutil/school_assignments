#ifndef VISION_ASSIGNMENTS_ROBUTIL_H
#define VISION_ASSIGNMENTS_ROBUTIL_H

#include <iostream>
#include <vector>
#include <sys/stat.h>

class Robutil {
public:
    std::vector<std::string> listdir(std::string path);

    std::vector<std::string> split_string(const std::string &str, const std::string &delimiter);

    bool has(const std::vector<std::string> &items, const std::string &item);

    std::string getCleanName(const std::string &raw_name);

    std::string getCleanNameNoExt(const std::string &raw_name);

    bool checkIfFileExists(const std::string &name);

    void log(const std::vector<std::string> &items);

    std::string getFileContentsAsString(const std::string &filename);
};


#endif //VISION_ASSIGNMENTS_ROBUTIL_H
