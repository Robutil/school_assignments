#ifndef VISION_ASSIGNMENTS_CHECKIMAGE_H
#define VISION_ASSIGNMENTS_CHECKIMAGE_H

#include "Robutil.h"
#include "../detection/FaceDetector.h"

class CheckImage {
private:
    Robutil robutil;
    FaceDetector faceDetector;

public:
    CheckImage();

    void checkFolder(const std::string &folder_name);

    void check(const std::string &image_path, const std::string &result_path);

    std::vector<cv::Mat> checkForFaces(const std::string &filename);

    cv::Mat cropImage(const cv::Mat &img, const cv::Rect &target);
};


#endif //VISION_ASSIGNMENTS_CHECKIMAGE_H
