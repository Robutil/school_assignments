#include <opencv2/opencv.hpp>

#include "CheckImage.h"
#include "../../main.h"

CheckImage::CheckImage() {
}

void CheckImage::checkFolder(const std::string &folder_name) {
    std::string path = "/home/robbin/Documents/School/year_4/human_machine_interfaces/vision_assignments/resources/";
    std::vector<std::string> files = robutil.listdir(path + folder_name);
    std::string output_path = path + "checked/";

    for (const std::string &img_name: files) {
        check(img_name, output_path);
    }
}

void CheckImage::check(const std::string &image_path, const std::string &result_path) {
    cv::Mat img = cv::imread(image_path);
    std::string image_name = robutil.getCleanName(image_path);

    std::vector<cv::Rect> detectedFaces = faceDetector.checkForFaces(img);
    std::cout << image_name << ", detected " << detectedFaces.size() << " face(s)\n";

    int counter = 0;
    for (const cv::Rect &face: detectedFaces) {
        if (face.width > 0 && face.height > 0) {
            counter++;

            //Cut image
            cv::Mat cropped_image = img(face);
            cv::Size fast_crop(FORMATTED_IMG_WIDTH, FORMATTED_IMG_HEIGHT);
            cv::resize(cropped_image, cropped_image, fast_crop);

            //Store image
            std::cout << result_path + std::to_string(counter) + "_" + image_name << "\n";
            cv::imwrite(result_path + std::to_string(counter) + "_" + image_name, cropped_image);
        }
    }
}

std::vector<cv::Mat> CheckImage::checkForFaces(const std::string &filename) {
    cv::Mat img = cv::imread(filename);

    std::vector<cv::Rect> detectedFaces = faceDetector.checkForFaces(img);
    std::cout << "[CheckImage](checkForFaces): " << filename << ", detected " << detectedFaces.size() << " face(s)\n";

    std::vector<cv::Mat> faces;
    for (const cv::Rect &face: detectedFaces) {
        if (face.width > 0 && face.height > 0) {

            //Cut image
            cv::Mat cropped_image = img(face);
            cv::Size fast_crop(FORMATTED_IMG_WIDTH, FORMATTED_IMG_HEIGHT);
            cv::resize(cropped_image, cropped_image, fast_crop);
            cv::cvtColor(cropped_image, cropped_image, cv::COLOR_BGR2GRAY);

            //Keep result
            faces.push_back(cropped_image);
        }
    }

    return std::move(faces);
}

cv::Mat CheckImage::cropImage(const cv::Mat &img, const cv::Rect &target) {
    cv::Mat cropped_image = img(target);
    cv::Size fast_crop(FORMATTED_IMG_WIDTH, FORMATTED_IMG_HEIGHT);
    cv::resize(cropped_image, cropped_image, fast_crop);

    return std::move(cropped_image);
}
