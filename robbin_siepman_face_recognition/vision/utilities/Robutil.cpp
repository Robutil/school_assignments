#include <dirent.h>
#include <fstream>

#include "Robutil.h"

std::vector<std::string> Robutil::listdir(std::string path) {
    std::vector<std::string> names;

    //Format correct path
    if (path[path.size() - 1] != '/') path.push_back('/');

    //Open directory
    DIR *dir = nullptr;
    dirent *ent;
    if ((dir = opendir(path.c_str())) != 0) {
        while ((ent = readdir(dir)) != 0) {
            //Ignore default directory entries
            if (ent->d_name[0] != '.') {
                //Get the filename
                std::string helper = path + ent->d_name;
                names.push_back(helper); //store
            }
        }
        closedir(dir);
    }

    return std::move(names);
}

bool Robutil::has(const std::vector<std::string> &items, const std::string &item) {
    for (const std::string &i: items) {
        if (i == item) {
            return true;
        }
    }
    return false;
}

/**
 * Gets clean name, removes abs path
 * */
std::string Robutil::getCleanName(const std::string &raw_name) {
    unsigned long start_index, end_index = raw_name.size();
    if (raw_name[raw_name.size() - 1] == '/') {
        end_index--;
    }

    for (auto i = end_index - 1; i >= 0; --i) {
        if (raw_name[i] == '/') {
            return raw_name.substr(i + 1, end_index);
        }
    }
    return nullptr;
}

/**
 * Gets name without extension
 * */
std::string Robutil::getCleanNameNoExt(const std::string &raw_name) {
    std::string name_with_extension = getCleanName(raw_name);

    for (auto i = raw_name.size() - 1; i >= 0; --i) {
        if (raw_name[i] == '.') {
            return raw_name.substr(i + 1, raw_name.size());
        }
    }
    return nullptr;
}

bool Robutil::checkIfFileExists(const std::string &name) {
    struct stat buffer{};
    return (stat(name.c_str(), &buffer) == 0);
}

void Robutil::log(const std::vector<std::string> &items) {
    for (const auto &item: items) {
        std::cout << item << "\n";
    }
}

std::string Robutil::getFileContentsAsString(const std::string &filename) {
    std::ifstream ifs(filename);
    std::string content((std::istreambuf_iterator<char>(ifs)),
                        (std::istreambuf_iterator<char>()));
    return std::move(content);
}

std::vector<std::string> Robutil::split_string(const std::string &str, const std::string &delimiter) {
    std::vector<std::string> strings;

    std::string::size_type pos = 0;
    std::string::size_type prev = 0;
    while ((pos = str.find(delimiter, prev)) != std::string::npos) {
        strings.push_back(str.substr(prev, pos - prev));
        prev = pos + 1;
    }

    // To get the last substring (or only, if delimiter is not found)
    strings.push_back(str.substr(prev));

    return std::move(strings);
}
