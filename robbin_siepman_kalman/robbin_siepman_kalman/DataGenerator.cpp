#include <cstdlib>
#include <ctime>
#include <iostream>

#include "DataGenerator.h"

DataGenerator::DataGenerator(const int mean, const int deviation) : mean(mean), deviation(deviation) {}

std::vector<double> DataGenerator::generate(int samples) {
    std::vector<double> data;

    srand((unsigned) time(0));
    double rand_dev = mean - deviation;
    double random_number;
    for (int i = 0; i < samples; ++i) {
        double f = (double) rand() / RAND_MAX;
        data.push_back(rand_dev + f * (deviation * 2));
    }

    return std::move(data);
}
