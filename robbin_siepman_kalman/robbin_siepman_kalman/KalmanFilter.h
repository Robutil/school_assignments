#ifndef INVERSE_KINEMATICS_KALMANFILTER_H
#define INVERSE_KINEMATICS_KALMANFILTER_H

#include <vector>

class KalmanFilter {
private:
    double kalman_gain;
    double error_in_the_estimate;
    double error_in_the_measured_value;
    double estimated_value;

public:
    KalmanFilter(double error_in_the_estimate, double error_in_the_measured_value);

    KalmanFilter(double error_in_the_estimate, double error_in_the_measured_value, double estimated_value);

    std::vector<double> kalman_filter_values;
    double calculateNewValue(double measured_value);
};


#endif //INVERSE_KINEMATICS_KALMANFILTER_H
