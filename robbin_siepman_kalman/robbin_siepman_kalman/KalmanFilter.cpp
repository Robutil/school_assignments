#include "KalmanFilter.h"

KalmanFilter::KalmanFilter(double error_in_the_estimate, double error_in_the_measured_value) : error_in_the_estimate(
        error_in_the_estimate), error_in_the_measured_value(error_in_the_measured_value) {
    estimated_value = 1;
}

KalmanFilter::KalmanFilter(double error_in_the_estimate, double error_in_the_measured_value, double estimated_value)
        : error_in_the_estimate(error_in_the_estimate), error_in_the_measured_value(error_in_the_measured_value),
          estimated_value(estimated_value) {}

double KalmanFilter::calculateNewValue(double measured_value) {
    kalman_gain = error_in_the_estimate / (error_in_the_estimate + error_in_the_measured_value);

    estimated_value = estimated_value + kalman_gain * (measured_value - estimated_value);

    error_in_the_estimate = (1 - kalman_gain) * error_in_the_estimate;

    kalman_filter_values.push_back(estimated_value);
    return estimated_value;
}
