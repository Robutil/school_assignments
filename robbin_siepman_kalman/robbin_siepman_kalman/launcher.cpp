#include <vector>
#include <iostream>

#include "DataGenerator.h"
#include "KalmanFilter.h"

int main() {
    DataGenerator dataGenerator(60, 5);
    std::vector<double> data = dataGenerator.generate(15);
    KalmanFilter kalmanFilter(5, 5, 60);

    std::cout << "[n]:\tData values:\tKalman values:\n";
    for (int i = 0; i < data.size(); ++i) {
        std::cout << "[" << i << "]:\t" << data[i] << "\t\t\t";
        std::cout << kalmanFilter.calculateNewValue(data[i]) << "\n";
    }
}

