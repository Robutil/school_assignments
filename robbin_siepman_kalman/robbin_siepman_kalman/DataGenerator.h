#ifndef INVERSE_KINEMATICS_DATAGENERATOR_H
#define INVERSE_KINEMATICS_DATAGENERATOR_H


#include <vector>

class DataGenerator {
private:
    const int mean;
    const int deviation;

public:
    DataGenerator(int mean, int deviation);

    std::vector<double> generate(int samples);
};


#endif //INVERSE_KINEMATICS_DATAGENERATOR_H
