To run: open terminal in this folder.

run cmake to generate makefile:
cmake .

make the executable:
make siepman_ccd

run the executable
./siepman_ccd

To change the target coordinate, provide a new coordinates as
command line parameters, for example:

./siepman_ccd -4.5 6.0

===Troubleshooting===
requires python 2.7 for visual stuff
