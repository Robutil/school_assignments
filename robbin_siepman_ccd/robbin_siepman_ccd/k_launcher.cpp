#include <iostream>

#include "core/ArmSegment.h"
#include "core/Ccd.h"

#define VERBOSE

#ifdef VERBOSE

#include "utils/matplotlibcpp.h"

namespace plt = matplotlibcpp;
#endif

//Default target to reach
#define DEFAULT_TARGET_X 4.5
#define DEFAULT_TARGET_Y 6.0

//Default arm constants
#define NUMBER_OF_ARM_SEGMENTS 3
#define DEFAULT_ARM_LENGTH 3.0
#define ARM_STARTING_ANGLE 0

//Default constraints for armSegments
#define DEFAULT_CONSTRAINT 0.785398
#define DEFAULT_DAMPING 0.1

int main(int argc, char *argv[]) {
    //Set default target values
    double target_x = DEFAULT_TARGET_X;
    double target_y = DEFAULT_TARGET_Y;

    //Check if user overwrote target values
    if (argc == 3) {
        target_x = atof(argv[1]); // NOLINT
        target_y = atof(argv[2]); // NOLINT
    } else if (argc != 1) {
        std::cerr << "Usage: " << argv[0] << " TARGET_X_VALUE TARGET_Y_VALUE";
        return 1;
    } else {
        //Give the user a hint on how to overwrite default coordinates
        std::cout << "Overwrite default target coordinates by providing a new X and Y as command line parameters\n";
    }

    //Set target
    Point pointToReach(target_x, 0.0, target_y);
    std::cout << "Will try to reach point x: " << target_x << ", y: " << target_y << "\nApplying a damping of: "
              << DEFAULT_DAMPING << " radians, and setting constraints per joint (skipping base) to "
              << DEFAULT_CONSTRAINT << " radians" << std::endl;

    //Create arm segments
    auto *armSegments = new ArmSegment[NUMBER_OF_ARM_SEGMENTS];
    for (int i = 0; i < NUMBER_OF_ARM_SEGMENTS; ++i) {
        armSegments[i] = ArmSegment(DEFAULT_ARM_LENGTH);

        //Set damping for more fluent movements
        armSegments[i].damping = DEFAULT_DAMPING;

        if (i != 0) {
            //Set joint constraints for all arm segments 45 degrees, but skip the base arm segment (seemed more natural)
            armSegments[i].negativeConstraint = -DEFAULT_CONSTRAINT;
            armSegments[i].positiveConstraint = DEFAULT_CONSTRAINT;
        }

        //Register callback, used to iterate updates
        if (i > 0) armSegments[i - 1].registerChildArmSegment(&armSegments[i]);
    }

    /* Connect the root armSegment to the ground, use that to calculate other positions
     * Note: that every update() applies already set constraints, to avoid this and set
     * the arm in a fixed starting position; set constraints later. */
    Point ground;
    armSegments[0].update(&ground, ARM_STARTING_ANGLE);

#ifdef VERBOSE
    std::cout << "\nStarting positions dump (per armSegment): \n";
    for (int i = 0; i < NUMBER_OF_ARM_SEGMENTS; ++i) {
        armSegments[i].dump();
    }
#endif

    //Apply Cyclic Coordinate Descent
    Ccd ccd = Ccd(armSegments, NUMBER_OF_ARM_SEGMENTS);
    bool success = ccd.computeCcdLink(pointToReach);

#ifdef VERBOSE
    std::cout << "\nEnd positions dump (per armSegment): \n";
    std::vector<double> x_arm_points;
    std::vector<double> y_arm_points;
    x_arm_points.push_back(0.0);
    y_arm_points.push_back(0.0);

    for (int i = 0; i < NUMBER_OF_ARM_SEGMENTS; ++i) {
        x_arm_points.push_back(armSegments[i].end.x);
        y_arm_points.push_back(armSegments[i].end.z);
        armSegments[i].dump();
    }
#endif

    //Show whether the target was reached
    std::cout << "The target was reached: " << (success ? "True" : "False") << "\n";

#ifdef VERBOSE
    plt::plot(x_arm_points, y_arm_points);
    plt::axis("equal");
    plt::show();
#endif

    //Cleanup
    delete[] armSegments;
    return 0;
}