#ifndef INVERSE_KINEMATICS_ARMSEGMENT_H
#define INVERSE_KINEMATICS_ARMSEGMENT_H


#include "Point.h"

class ArmSegment {
private:
    double length;
    double angle_ground = 0.0;
    double angle_joint = 0.0;

    ArmSegment *childArmSegment = nullptr;

    void calculateNewEndPoint();

public:
    double damping = 6.28319;
    double negativeConstraint = -6.28319;
    double positiveConstraint = 6.28319;
    Point origin;
    Point end;

    ArmSegment();

    explicit ArmSegment(double length);

    ArmSegment(double length, double angle);

    ArmSegment(const Point &origin, double length);

    ArmSegment(const Point &origin, double length, double angle);

    void registerChildArmSegment(ArmSegment *armSegment);

    void update(double angleTranslation);

    void update(Point *newOrigin, double angleTranslation);

    void dump();
};


#endif //INVERSE_KINEMATICS_ARMSEGMENT_H
