#ifndef INVERSE_KINEMATICS_CCD_H
#define INVERSE_KINEMATICS_CCD_H

#include "Point.h"
#include "ArmSegment.h"

#define AMOUNT_OF_TRIES 10000
#define TARGET_REACHED_THRESHOLD 0.001

class Ccd {
private:
    ArmSegment *armSegments;
    const unsigned int AMOUNT_OF_LINKS;

public:
    Ccd(ArmSegment *armSegments, unsigned int AMOUNT_OF_LINKS);

    bool computeCcdLink(Point targetPoint);
};


#endif //INVERSE_KINEMATICS_CCD_H
