#include <cmath>
#include <iostream>

#include "ArmSegment.h"

ArmSegment::ArmSegment() = default;

ArmSegment::ArmSegment(double length) : length(length) {}

ArmSegment::ArmSegment(double length, double angle) : length(length), angle_ground(angle) {}

ArmSegment::ArmSegment(const Point &origin, double length, double angle) : origin(origin), length(length),
                                                                           angle_ground(angle) {}

ArmSegment::ArmSegment(const Point &origin, double length) : origin(origin), length(length) {}

/**
 * Adds another armSegment as a child to this armSegment. That means
 * that every change to this armSegment will also affect children of
 * this armSegment. An armSegment can only have one child, but it is
 * a recursive process.
 * */
void ArmSegment::registerChildArmSegment(ArmSegment *armSegment) {
    this->childArmSegment = armSegment;
}

/**
 * Recalculates the armSegment's end-point using the same origin but
 * a different angle. This means that this argSegment is the cause of
 * a rotation.
 * */
void ArmSegment::update(double angleTranslation) {
    update(nullptr, angleTranslation);
}

/**
 * Recalculates the armSegment's end-point using a new origin and angle.
 * If newOrigin == nullptr it means that we are the cause of an rotation.
 * */
void ArmSegment::update(Point *newOrigin, double angleTranslation) {
    if (newOrigin == nullptr) {
        //Apply constraints
        if ((angle_joint + angleTranslation) < negativeConstraint) {
            angleTranslation = negativeConstraint + fabs(this->angle_joint);
        } else if ((angle_joint + angleTranslation) > positiveConstraint) {
            angleTranslation = positiveConstraint - fabs(this->angle_joint);
        }

        //Update joint angle (can only be done if we are the origin of a rotation)
        angle_joint += angleTranslation;
    }

    //Update ground angle
    angle_ground += angleTranslation;

    //Update new position, if required
    if (newOrigin != nullptr) {
        origin = *newOrigin;
    }

    //Update end-point coordinates based on new angle and start-point
    calculateNewEndPoint();

    //Update child armSegments, if any
    if (childArmSegment != nullptr) {
        childArmSegment->update(&end, angleTranslation);
    }
}

/**
 * Uses the private angle variable to perform geometric calculations
 * and discover the armSegment's end-point.
 * */
void ArmSegment::calculateNewEndPoint() {
    double x_translate = cos(angle_ground) * length;
    double z_translate = sin(angle_ground) * length;

    end.x = origin.x + x_translate;
    end.z = origin.z + z_translate;
}

/**
 * Outputs armSegment data.
 * */
void ArmSegment::dump() {
    std::cout << "==================\nAngle joint: " << angle_joint << "\tAngle ground: " << angle_ground << std::endl;
    origin.dump();
    end.dump();
    std::cout << "==================\n";
}
