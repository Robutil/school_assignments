#ifndef INVERSE_KINEMATICS_KVECTOR_H
#define INVERSE_KINEMATICS_KVECTOR_H


#include "Point.h"

class Vector3 {
private:
    Point origin;
public:
    Point end;

    explicit Vector3(Point end);

    Vector3(Point origin, Point end);

    void normalize();

    void dump();
};


#endif //INVERSE_KINEMATICS_KVECTOR_H
