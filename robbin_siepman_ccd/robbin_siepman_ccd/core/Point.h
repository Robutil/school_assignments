#ifndef INVERSE_KINEMATICS_KPOINT_H
#define INVERSE_KINEMATICS_KPOINT_H


class Point {
public:
    double x = 0.0;
    double y = 0.0;
    double z = 0.0;

    Point();

    Point(double x, double y, double z);

    void dump();
};


#endif //INVERSE_KINEMATICS_KPOINT_H
