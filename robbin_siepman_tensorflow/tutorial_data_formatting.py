import json
import numpy as np
import tensorflow as tf

from PIL import Image

data = []
labels = []

with open('data/labels.json') as label_file:
    content = label_file.read()
    json_labels = json.loads(content)['labels']

    for numb in json_labels:
        tensor_output = np.zeros(10)
        tensor_output[numb] = 1
        labels.append(tensor_output)

for i in range(len(labels)):
    image_content = np.array(Image.open('data/' + str(i + 1) + '.jpeg'))
    image_content = image_content.flatten()
    data.append(image_content)

assert len(data) == len(labels)
number_of_samples = len(data)

training_data = data[number_of_samples // 2:]
training_labels = labels[number_of_samples // 2:]

test_data = data[:number_of_samples // 2]
test_labels = labels[:number_of_samples // 2]

# Create the model
x = tf.placeholder(tf.float32, [None, len(data[0])], name='x')
y_ = tf.placeholder(tf.float32, [None, 10], name='y_')

W = tf.Variable(tf.zeros([len(data[0]), 10]))
b = tf.Variable(tf.zeros([10]))
y = tf.matmul(x, W) + b

cross_entropy = tf.reduce_mean(
    tf.nn.softmax_cross_entropy_with_logits(labels=y_, logits=y))
train_step = tf.train.GradientDescentOptimizer(0.5).minimize(cross_entropy)

sess = tf.InteractiveSession()
tf.global_variables_initializer().run()

# Train
sess.run(train_step, feed_dict={x: training_data, y_: training_labels})

# Test trained model
correct_prediction = tf.equal(tf.argmax(y, 1), tf.argmax(y_, 1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

print("Accuracy on test data: " + str(sess.run(accuracy, feed_dict={x: test_data, y_: test_labels}) * 100) + ' %')
