from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from sklearn import datasets
import tensorflow as tf
import numpy as np
import scipy.misc
import json

SAVE_DATA_SET = True

digits = datasets.load_digits()
number_of_samples = len(digits.images)

data = []
labels = []

counter = 1
for img in digits.images:
    data.append(img.flatten())

    if SAVE_DATA_SET:
        scipy.misc.imsave("data/" + str(counter) + ".jpeg", img)
        counter += 1

for label in digits.target:
    output = np.zeros(10)
    output[label] = 1
    labels.append(output)

if SAVE_DATA_SET:
    with open('data/labels.json', 'w') as label_file:
        output = {'labels': digits.target.tolist()}
        json.dump(output, label_file)

training_data = data[number_of_samples // 2:]
training_labels = labels[number_of_samples // 2:]

test_data = data[:number_of_samples // 2]
test_labels = labels[:number_of_samples // 2]

# Create the model
x = tf.placeholder(tf.float32, [None, len(data[0])], name='x')
y_ = tf.placeholder(tf.float32, [None, 10], name='y_')

W = tf.Variable(tf.zeros([len(data[0]), 10]))
b = tf.Variable(tf.zeros([10]))
y = tf.matmul(x, W) + b

cross_entropy = tf.reduce_mean(
    tf.nn.softmax_cross_entropy_with_logits(labels=y_, logits=y))
train_step = tf.train.GradientDescentOptimizer(0.5).minimize(cross_entropy)

sess = tf.InteractiveSession()
tf.global_variables_initializer().run()

# Train
batch_xs, batch_ys = training_data, training_labels
sess.run(train_step, feed_dict={x: batch_xs, y_: batch_ys})

# Test trained model
correct_prediction = tf.equal(tf.argmax(y, 1), tf.argmax(y_, 1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

print("Results:")
print(sess.run(accuracy, feed_dict={x: test_data, y_: test_labels}))

