#include "ImageLoader.h"

#include <dirent.h>

ImageLoader::ImageLoader(const std::string &project_location) : project_location(project_location) {}

bool ImageLoader::load(std::string &folder) {
    //Reset earlier vectors
    image_names.clear();
    images.clear();

    //Format correct path
    if (folder[folder.size() - 1] != '/') folder.push_back('/');
    std::string full_path = project_location + folder;

    //Open directory
    DIR *dir = nullptr;
    dirent *ent;
    if ((dir = opendir(full_path.c_str())) != 0) {
        while ((ent = readdir(dir)) != 0) {
            //Ignore default directory entries
            if (ent->d_name[0] != '.') {
                //Get the filename
                std::string helper = full_path + ent->d_name;
                image_names.push_back(helper); //store
            }
        }
        closedir(dir);

        //Open images
        for (const auto &temp: image_names) {
            std::cout << "[ImageLoader](load): Found image: " << temp << "\n";

            Mat img = imread(temp);
            images.push_back(img);
        }
        return true;
    }

    //Directory does not exist
    std::cout << "[ImageLoader](load): Directory " << full_path << " not found.";
    return false;
}