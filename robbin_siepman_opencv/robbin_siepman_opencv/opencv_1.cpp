#include "assignments/AssignTwo.h"
#include "assignments/AssignOne.h"
#include "assignments/AssignThree.h"

#include "utilities/ImageLoader.h"

int main(int argc, char **argv) {
    std::string path, default_path = "robbin_siepman_opencv/resources/";
    std::cout << "Usage: " << argv[0] << " <path_to_resources_folder>\n";
    if (argc != 2) {
        std::cout << "No path provided, using default: " << default_path << std::endl;
        path = std::move(default_path);
    } else {
        path = argv[1];
    }

    ImageLoader imageLoader = ImageLoader(path);

    AssignOne assignOne(&imageLoader);
    assignOne.run();

    AssignTwo assignTwo(&imageLoader);
    assignTwo.run();

    AssignThree assignThree(&imageLoader);
    assignThree.run();

    return 0;
}