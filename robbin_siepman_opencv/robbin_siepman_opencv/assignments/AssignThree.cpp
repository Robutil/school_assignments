#include <opencv2/opencv.hpp>

#include "AssignThree.h"

using namespace cv;

AssignThree::AssignThree(ImageLoader *imageLoader) : Assignment(imageLoader) {}

void AssignThree::run() {
    std::cout << "\n=== Starting Assignment 3 === \n\n";

    //First load template
    std::string template_directory = "template";
    imageLoader->load(template_directory);
    if (imageLoader->images.empty()) return;
    Mat template_image = imageLoader->images[0];

    //Then load assignment images
    std::string directory_name = "3";
    imageLoader->load(directory_name);
    std::cout << std::endl;

    Mat img, gray;
    for (const auto &fn: imageLoader->image_names) {
        img = imread(fn, 1);

        //Make the image gray, 8 bit color
        cvtColor(img, gray, COLOR_BGR2GRAY);

        Mat result;
        matchTemplate(img, template_image, result, TM_CCOEFF);
        normalize(result, result, 0, 1, NORM_MINMAX, -1, Mat());

        double minVal;
        double maxVal;
        Point minLoc;
        Point maxLoc;
        Point matchLoc;

        minMaxLoc(result, &minVal, &maxVal, &minLoc, &maxLoc, Mat());

        matchLoc = maxLoc;

        bool correct = false;
        /// Show me what you got
        if (matchLoc.x + template_image.cols > 130 && matchLoc.x + template_image.cols < 150) {
            rectangle(img, matchLoc, Point(matchLoc.x + template_image.cols, matchLoc.y + template_image.rows),
                      Scalar::all(0), 2, 8,
                      0);
            correct = true;
        }

        std::string correct_format = "no";
        if (correct) correct_format = "yes";
        std::cout << "File: " << fn << "\nScrew is inserted correctly: " << correct_format << "\n---\n";

//        namedWindow("Display Image", WINDOW_AUTOSIZE);
//        imshow("Display Image", img);
//        waitKey(600);
    }
}


