#ifndef INVERSE_KINEMATICS_ASSIGNMENT_H
#define INVERSE_KINEMATICS_ASSIGNMENT_H

#include <iostream>
#include "../utilities/ImageLoader.h"

class Assignment {
public:
    ImageLoader *imageLoader;

    explicit Assignment(ImageLoader *imageLoader) : imageLoader(imageLoader) {}

    virtual void run()= 0;
};


#endif //INVERSE_KINEMATICS_ASSIGNMENT_H
