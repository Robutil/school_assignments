#include <opencv2/opencv.hpp>

#include "AssignOne.h"

using namespace cv;

struct Line {
    int x_start;
    int y_start;
    int x_end;
    int y_end;
};

AssignOne::AssignOne(ImageLoader *imageLoader) : Assignment(imageLoader) {}

void AssignOne::run() {
    std::cout << "\n=== Starting Assignment 1 === \n\n";
    std::string directory_name = "1";
    imageLoader->load(directory_name);
    std::cout << std::endl;

    Mat img, gray;
    for (const auto &fn: imageLoader->image_names) {
        img = imread(fn, 1);

        //Make the image gray, 8 bit color
        cvtColor(img, gray, COLOR_BGR2GRAY);

        //Make the image blurry, to ease edge detection
        GaussianBlur(gray, gray, Size(9, 9), 2, 2);

        Mat edges;
        Canny(gray, edges, 180, 240);

        std::vector<Vec2f> lines;
        HoughLines(edges, lines, 1, M_PI / 180, 44);

        int linesFound = 0;
        for (auto &line : lines) {

            //Convert line formulate to line
            float rho = line[0];
            float theta = line[1];
            double a = std::cos(theta), b = std::sin(theta);
            double x0 = a * rho, y0 = b * rho;

            Line ln = {
                    abs(cvRound(x0 + 1000 * (-b))),
                    abs(cvRound(y0 + 1000 * (a))),
                    abs(cvRound(x0 - 1000 * (-b))),
                    abs(cvRound(y0 - 1000 * (a)))
            };


            if (abs(ln.y_start - ln.y_end) > 30) {
                linesFound++;
            }
        }

//        namedWindow("Display Image", WINDOW_AUTOSIZE);
//        imshow("Display Image", edges);
//        waitKey(600);

        //Print output
        std::string found = "no";
        if (linesFound >= 8) found = "yes";
        std::cout << "File: " << fn << "\nFound " << linesFound <<
                  " lines by edge. Valid connector: " << found << ".\n---\n";

    }
}
