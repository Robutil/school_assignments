#ifndef INVERSE_KINEMATICS_ASSIGNTHREE_H
#define INVERSE_KINEMATICS_ASSIGNTHREE_H


#include "Assignment.h"

class AssignThree : public Assignment{
public:
    AssignThree(ImageLoader *imageLoader);

    void run();
};


#endif //INVERSE_KINEMATICS_ASSIGNTHREE_H
