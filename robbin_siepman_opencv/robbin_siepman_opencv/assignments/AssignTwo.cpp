#include <opencv2/opencv.hpp>

#include "AssignTwo.h"

using namespace cv;

AssignTwo::AssignTwo(ImageLoader *imageLoader) : Assignment(imageLoader) {}

struct CircleCenter {
    int x;
    int y;
};

std::vector<Vec3f> houghDetection(const Mat &src_gray, int cannyThreshold, int accumulatorThreshold) {
    std::vector<Vec3f> circles;
    HoughCircles(src_gray, circles, HOUGH_GRADIENT, 1, src_gray.rows / 8, cannyThreshold, accumulatorThreshold, 0, 0);
    return circles;
}

void AssignTwo::run() {
    std::cout << "\n=== Starting Assignment 2 === \n\n";
    std::string directory_name = "2";
    imageLoader->load(directory_name);
    std::cout << std::endl;

    Mat img, gray;
    for (const auto &fn: imageLoader->image_names) {
        img = imread(fn, 1);

        //Make the image gray, 8 bit color
        cvtColor(img, gray, COLOR_BGR2GRAY);

        //Make the image blurry, to ease edge detection
        GaussianBlur(gray, gray, Size(9, 9), 2, 2);

        int cannyThreshold = 100;
        int accumulatorThreshold = 20;
        auto circles = houghDetection(gray, cannyThreshold, accumulatorThreshold);

        if (circles.size() > 2) {
            //Get circle centers from Hough
            struct CircleCenter one = {
                    cvRound(circles[0][0]),
                    cvRound(circles[0][1])
            };
            struct CircleCenter two = {
                    cvRound(circles[1][0]),
                    cvRound(circles[1][1])
            };

            //Pythagoras for accurate distance
            int x_distance = abs(one.x - two.x);
            int y_distance = abs(one.y - two.y);
            double distance = sqrt(pow(x_distance, 2) + pow(y_distance, 2));

            //Print output
            std::cout << "File: " << fn << "\nHas a distance of: " << distance << "\n---\n";
        }
    }
}
