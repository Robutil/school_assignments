#ifndef INVERSE_KINEMATICS_ASSIGNONE_H
#define INVERSE_KINEMATICS_ASSIGNONE_H

#include "Assignment.h"

class AssignOne : public Assignment {
public:

    explicit AssignOne(ImageLoader *imageLoader);

    void run() override;
};


#endif //INVERSE_KINEMATICS_ASSIGNONE_H
