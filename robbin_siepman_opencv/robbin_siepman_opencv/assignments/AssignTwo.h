#ifndef INVERSE_KINEMATICS_ASSIGNTWO_H
#define INVERSE_KINEMATICS_ASSIGNTWO_H

#include "Assignment.h"

class AssignTwo : public Assignment {
public:

    explicit AssignTwo(ImageLoader *imageLoader);

    void run() override;
};


#endif //INVERSE_KINEMATICS_ASSIGNTWO_H
