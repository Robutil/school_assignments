To run: open terminal in this folder.

run cmake to generate makefile:
cmake .

make the executable:
make siepman_opencv

run the executable
./siepman_opencv

===Troubleshooting===
If the images cannot be found, provide the correct path to the resources folder located in the robbin_siepman_opencv folder using the command, end with a slash:
./siepman_opencv <full_path_to_resources_folder>
For example:
./siepman_opencv /home/your_name/robbin_siepman_opencv/resources/
